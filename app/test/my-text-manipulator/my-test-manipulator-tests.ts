/* No TS definitions for WebComponentTester, so use declare. */
 declare var fixture;
 declare var flush;
 declare var stub;

/**
 * Tests for my-text-manipulator.
 */
describe('my-text-manipulator tests', function () {
	let element: MyApp.TextManipulator;
	let originalText = "Testing is fun!"

	before(function () {
		element = fixture('my-text-manipulator');
	});

	after(function () {
	});

	beforeEach(function () {
	});

	afterEach(function () {
	});

	it('should be instantiated', function() {
		chai.expect(element.is).equals('my-text-manipulator');
	});

	it('make sure one item is displayed by default', function() {
		element.$.repeater.render();
		chai.expect(element.$.repeater.renderedItemCount).equals(1);
	});

	it('make sure the proper number of items are changed when numberOfRepetitions is set', function() {
		element.numberOfRepetitions = 5;
		element.$.repeater.render();
		chai.expect(element.$.repeater.renderedItemCount).equals(5);
	});
});

