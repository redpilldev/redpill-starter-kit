/* No TS definitions for WebComponentTester, so use declare. */
declare var fixture;
declare var flush;
declare var stub;

/**
 * Tests for my-view1.
 */
describe('my-view1 tests', function () {
	let view1:SimpleViewTester;

	before(function () {
		view1 = new SimpleViewTester(fixture('my-view1'));
	});

	beforeEach(function (done) {
		view1.init(done);
	});

	afterEach(function (done) {
		view1.reset(done);
	});

	it('Should display "1" inside of the circle', function () {
		const circle:HTMLDivElement = view1.circle;
		chai.expect(circle).to.not.be.null;
		chai.expect(circle.textContent).equals('1');
	})
});
