namespace MyApp {

	/**
	 * Sample starter application. Rename this class and namespace appropriately and add any application-specific functionality here.
	 * The base class will add a new instance of this class to the window object under the name 'app'.
	 * Refer to the base classes ({@link NowElements.BaseApp} and {@link NowElements.BasicApp}) to understand the default functionality.
	 *
	 * @author Kito D. Mann
	 */
	@component('my-app')
	export class App extends NowElements.BasicApp {

		ready() {
			super.ready();
		}

		attached() {
			super.attached();
		}
	}
}

MyApp.App.register();

