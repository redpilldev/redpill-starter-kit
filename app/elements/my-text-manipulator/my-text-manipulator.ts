namespace MyApp {

	/**
	 * Simple example application-specific component that can reverse is contents and repeat it for a certain number
	 * of times.
	 *
	 * @author Kito D. Mann
	 */
	@component('my-text-manipulator')
	export class TextManipulator extends NowElements.BaseElement {

		@property({type: String})
		text: String;

		@property({type: Boolean, value: false})
		reverse: boolean;

		@property({type: Number, value: 1})
		numberOfRepetitions: number;

		@computed({type: String})
		displayText(reverse: boolean, text: string) {
			return reverse ? this._reverseString(text) : text;
		}

		@computed({type: Array})
		counterArray(numberOfRepetitions: number) {
			let array = new Array();
			for (let i = 0; i < numberOfRepetitions; i++) {
				array.push(i);
			}
			return array;
		}

		ready() {
			console.debug(this.is, "ready!");
		}

		attached() {
			console.debug(this.is, "attached!");
		}

		private _reverseString(s: string) {
			return (s === '') ? '' : this._reverseString(s.substr(1)) + s.charAt(0);
		}
	}
}

MyApp.TextManipulator.register();
