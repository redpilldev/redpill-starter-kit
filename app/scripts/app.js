//Ensure user logged in
var origOpen = XMLHttpRequest.prototype.open;
XMLHttpRequest.prototype.open = function() {
	//console.log('XMLHttpRequest.open, args=', arguments);
	this.addEventListener('load', function(xhr) {
		//console.log('XMLHttpRequest.onLoad, args=', arguments);
		var response = xhr.target.response;
		//console.log('XMLHttpRequest.onLoad, response=', response);
		if (response && 'exceptiontype' in response) {
			if (response.exceptiontype === 'UserAccessException') {
				location.href = '/names.nsf?login&redirectto=' + location.href;
			}
		}
	});
	origOpen.apply(this, arguments);
	this.setRequestHeader('cache-control', 'max-age=0');
};

/*var origOpen = XMLHttpRequest.prototype.open;
 XMLHttpRequest.prototype.open = function() {
 //console.log('XMLHttpRequest.open, args=', arguments);
 var that = this;
 this.addEventListener('loadend', function(xhr) {
 //console.log('XMLHttpRequest.onLoad, args=', arguments);
 //console.log('all headers=', this.getAllResponseHeaders());
 var responseType = this.getResponseHeader('content-type');
 var response = xhr.target.response;
 var responseTxt = xhr.responseText;
 //console.log('XMLHttpRequest.onLoad, response=', response);
 if (response && 'exceptiontype' in response) {
 if (response.exceptiontype === 'UserAccessException') {
 location.href = '/names.nsf?login&redirectto=' + location.href;
 }
 }else if (!response && responseType.indexOf('text/html') > -1) {
 var serverHeader = this.getResponseHeader('server');
 if (serverHeader === 'Lotus-Domino') {
 location.href = '/names.nsf?login&redirectto=' + location.href;
 }
 }
 });
 origOpen.apply(this, arguments);
 this.setRequestHeader('cache-control', 'max-age=0');
 };*/

/* [8/25/16, 4:36:42 PM] Keith Strickland: if (responseText) {
					var response = responseText.toLowerCase();
					var authUrl = Settings.AUTHENTICATION_URL.toLowerCase();
					if (response.indexOf(authUrl) > -1) {
						location.href = authUrl + '&redirectto=' + location.href;
					}
				}
 [8/25/16, 4:37:08 PM] Keith Strickland: define({
	BASE_URL: '/thq/cep2.nsf',
	REST_FETCH_LIMIT: 50,
	QUERY_FETCH_LIMIT: 10,
	AUTHENTICATION_URL: '/names.nsf?login',
	LOGOUT_URL: '/names.nsf?logout',
	WINDOW_TITLE: 'CEP 2.0',
	DEFAULT_HEADER: 'Corps Evaluation and Planning',
	INFINITE_SCROLL_TRIGGER: 300
});
 */
